#define COMBO_IN 1
#define OUT_COMBO 0

unsigned long buff;
int st_old, combo_f;
unsigned long prev;


void setup() {
  Serial.begin(9600);
  pinMode(13, OUTPUT);
  pinMode(12, INPUT);
  digitalWrite(13, HIGH);

  buff = 0;
  st_old = 0;
  prev = 0;
  combo_f = OUT_COMBO;
  Serial.print("test");
}

void loop() {
  unsigned long now;
  int  i,st;
  st = digitalRead(12);
  while(st == st_old) {
    st = digitalRead(12);
  }
  i = st;
  now = micros();
  if((now - prev) < 200000) {
    i |= 2;
  } else {
    combo_f = OUT_COMBO;
  }
  if((now - prev) > 10000) {
    if(combo_f == OUT_COMBO) {
      if(st==HIGH) Serial.print(0);
      else Serial.print(1);
    }
    buff = (buff<<2) | i;
    if((buff&0x02fff) == 0x0eee) {
      combo_f = COMBO_IN;
      Serial.print("s");
    } else if((buff&0x02fff) == 0x0bbb) {
      combo_f = COMBO_IN;
      Serial.print("s");
    } else if((buff&0xbfffffff) == 0x2eeeeeee) {
      Serial.print("S");
    } else if((buff&0xbfffffff) == 0x3bbbbbbb) {
      Serial.print("S");
    }
//    Serial.print(',');
//    Serial.println(i);
    st_old = st;
    prev = now;
  }
}
