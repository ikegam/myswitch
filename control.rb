require 'rubygems'
require 'mqtt'
require 'serialport'


class Device

  attr_accessor :status, :prev_status

  def initialize(port, baud=9600)
    @port = SerialPort.new(port, baud, 8, 1, 0)
    @port.flow_control = SerialPort::NONE
    #@port.read_timeout = 50
    @status = :off
    @thr = Thread.new {
      buf = nil
      while true
        buf = @port.read(1)
        case buf
        when "0"
          @status = :off
        when "1"
          @status = :on
        when "s"
          @prev_status = @status
          @status = :special1
        when "S"
          @status = :special2
        end
      end
    }
  end

end

dev = Device.new("/dev/ttyUSB0")
#dev = Device.new("/dev/ttyACM0")
c =  MQTT::Client.connect('mqtt://admin:password@100.123.45.194:61613/')

while true do
  status = dev.status
  #p status
  #p dev.prev_status
  p status
  case status
  when :on
    c.publish('ikegam/gs/mode', 'on')
    c.publish('3dbcs.biz/UTokyo/iREF/6F/Hilobby/Light/LED/LoE/L42/brightness/W', 'value=10')
  when :off
    c.publish('ikegam/gs/mode', 'off')
    c.publish('3dbcs.biz/UTokyo/iREF/6F/Hilobby/Light/LED/LoE/L42/brightness/W', 'value=0')
  when :special1
    if dev.prev_status == :on
      c.publish('ikegam/gs/mode', 'special1_on')
      Thread.new {
      }
      c.publish('3dbcs.biz/UTokyo/iREF/6F/Hilobby/Light/LED/LoE/L42/brightness/W', 'value=10')
      c.publish('3dbcs.biz/UTokyo/iREF/6F/Hilobby/Light/LED/LoE/L43/brightness/W', 'value=10')
      c.publish('3dbcs.biz/UTokyo/iREF/6F/Hilobby/Light/LED/LoE/L44/brightness/W', 'value=10')
    else
      c.publish('ikegam/gs/mode', 'special1_off')
      c.publish('3dbcs.biz/UTokyo/iREF/6F/Hilobby/Light/LED/LoE/L42/brightness/W', 'value=0')
      c.publish('3dbcs.biz/UTokyo/iREF/6F/Hilobby/Light/LED/LoE/L43/brightness/W', 'value=0')
      c.publish('3dbcs.biz/UTokyo/iREF/6F/Hilobby/Light/LED/LoE/L44/brightness/W', 'value=0')
    end
  when :special2
    if dev.prev_status == :on
      c.publish('ikegam/gs/mode', 'special2_on')
      c.publish('3dbcs.biz/UTokyo/iREF/6F/Hilobby/Light/LED/LoE/L42/brightness/W', 'value=10')
      c.publish('3dbcs.biz/UTokyo/iREF/6F/Hilobby/Light/LED/LoE/L43/brightness/W', 'value=10')
      c.publish('3dbcs.biz/UTokyo/iREF/6F/Hilobby/Light/LED/LoE/L44/brightness/W', 'value=10')

      c.publish('3dbcs.biz/UTokyo/iREF/6F/Hilobby/Light/LED/LoE/L45/brightness/W', 'value=10')
      c.publish('3dbcs.biz/UTokyo/iREF/6F/Hilobby/Light/LED/LoE/L46/brightness/W', 'value=10')
      c.publish('3dbcs.biz/UTokyo/iREF/6F/Hilobby/Light/LED/LoE/L47/brightness/W', 'value=10')

      c.publish('3dbcs.biz/UTokyo/iREF/6F/Hilobby/Light/LED/LoE/L48/brightness/W', 'value=10')
      c.publish('3dbcs.biz/UTokyo/iREF/6F/Hilobby/Light/LED/LoE/L49/brightness/W', 'value=10')
      c.publish('3dbcs.biz/UTokyo/iREF/6F/Hilobby/Light/LED/LoE/L50/brightness/W', 'value=10')

      c.publish('3dbcs.biz/UTokyo/iREF/6F/Hilobby/Light/LED/LoE/L51/brightness/W', 'value=10')
      c.publish('3dbcs.biz/UTokyo/iREF/6F/Hilobby/Light/LED/LoE/L52/brightness/W', 'value=10')
      c.publish('3dbcs.biz/UTokyo/iREF/6F/Hilobby/Light/LED/LoE/L53/brightness/W', 'value=10')
    else
      c.publish('ikegam/gs/mode', 'special2_off')
      c.publish('3dbcs.biz/UTokyo/iREF/6F/Hilobby/Light/LED/LoE/L42/brightness/W', 'value=0')
      c.publish('3dbcs.biz/UTokyo/iREF/6F/Hilobby/Light/LED/LoE/L43/brightness/W', 'value=0')
      c.publish('3dbcs.biz/UTokyo/iREF/6F/Hilobby/Light/LED/LoE/L44/brightness/W', 'value=0')

      c.publish('3dbcs.biz/UTokyo/iREF/6F/Hilobby/Light/LED/LoE/L45/brightness/W', 'value=0')
      c.publish('3dbcs.biz/UTokyo/iREF/6F/Hilobby/Light/LED/LoE/L46/brightness/W', 'value=0')
      c.publish('3dbcs.biz/UTokyo/iREF/6F/Hilobby/Light/LED/LoE/L47/brightness/W', 'value=0')

      c.publish('3dbcs.biz/UTokyo/iREF/6F/Hilobby/Light/LED/LoE/L48/brightness/W', 'value=0')
      c.publish('3dbcs.biz/UTokyo/iREF/6F/Hilobby/Light/LED/LoE/L49/brightness/W', 'value=0')
      c.publish('3dbcs.biz/UTokyo/iREF/6F/Hilobby/Light/LED/LoE/L50/brightness/W', 'value=0')

      c.publish('3dbcs.biz/UTokyo/iREF/6F/Hilobby/Light/LED/LoE/L51/brightness/W', 'value=0')
      c.publish('3dbcs.biz/UTokyo/iREF/6F/Hilobby/Light/LED/LoE/L52/brightness/W', 'value=0')
      c.publish('3dbcs.biz/UTokyo/iREF/6F/Hilobby/Light/LED/LoE/L53/brightness/W', 'value=0')
    end
  else
  end
  sleep(0.2)
end

# Publish example
#!/usr/bin/ruby
#
